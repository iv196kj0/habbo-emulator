package com.habbo.emulator;

import com.habbo.emulator.habbohotel.catalog.CatalogFactory;
import com.habbo.emulator.messaging.PacketHandler;
import com.habbo.emulator.util.logging.Logger;
import com.habbo.emulator.networking.GameServer;
import com.habbo.emulator.util.validator.figure.FigureValidator;

public class Emulator {

    private static void initialize() {
        FigureValidator.getInstance();
        CatalogFactory.getInstance();
        PacketHandler.getInstance();
        Logger.logHealth("Initialization complete.");
    }

    public static void main(String[] args) {
        initialize();
        new GameServer().run();
    }
}
