package com.habbo.emulator.habbohotel.catalog;

public class Catalog {

    private final String type;

    public Catalog(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
