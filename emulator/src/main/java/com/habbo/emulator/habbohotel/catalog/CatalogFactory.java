package com.habbo.emulator.habbohotel.catalog;

import com.habbo.emulator.persistence.DAOFactory;
import com.habbo.emulator.persistence.skeleton.catalog.CatalogNodeDAO;
import com.habbo.emulator.util.logging.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CatalogFactory {

    private static CatalogFactory instance;

    private final CatalogNodeDAO dao;

    private CatalogNode rootNode;

    private CatalogFactory(CatalogNodeDAO dao) {
        this.dao = dao;

        prepareRootPage();

        Logger.logHealth("CatalogFactory initialized.");
    }

    public static CatalogFactory getInstance() {
        if (instance == null) {
            instance = new CatalogFactory(DAOFactory.getDAOFactory().getCatalogNodeDAO());
        }
        return instance;
    }

    private void prepareRootPage() {
        rootNode = dao.findByName("root");
        prepareChildNodes(rootNode);
    }

    private void prepareChildNodes(CatalogNode parent) {
        List<CatalogNode> children = dao.findByParent(parent.getName());

        children.sort(Comparator.comparingInt(CatalogNode::getOrder));
        parent.setChildren(children);

        for (CatalogNode child : children) {
            prepareChildNodes(child);
        }
    }

    public CatalogNode getRootNode() {
        return rootNode;
    }

    public CatalogPage getCatalogPage(int id) {
        CatalogPage page = new CatalogPage(id, "default_3x3");
        List<CatalogItem> items = new ArrayList<>();
        items.add(new CatalogItem(10093, "anna_div_crnr_green"));
        page.setItems(items);
        return page;
    }
}
