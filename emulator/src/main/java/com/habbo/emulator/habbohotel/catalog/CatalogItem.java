package com.habbo.emulator.habbohotel.catalog;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.server.ServerSerializable;

public class CatalogItem implements ServerSerializable {

    private final int id;
    private final String name;

    public CatalogItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public void writeObject(ServerMessage message) {
        message.writeInt(id);
        message.writeString(name);
        message.writeBoolean(false); // unknown
        message.writeInt(1); // credits
        message.writeInt(0); // points
        message.writeInt(0); // points type
        message.writeBoolean(true); // giftable

        message.writeInt(1); // item count
        message.writeString("s"); // item type
        message.writeInt(3977); // item sprite
        message.writeString(""); // item extradata

        message.writeBytes(new byte[] { 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0 }); // unknown
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
