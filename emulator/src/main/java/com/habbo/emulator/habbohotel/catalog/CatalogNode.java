package com.habbo.emulator.habbohotel.catalog;

import com.habbo.emulator.messaging.server.ServerSerializable;
import com.habbo.emulator.messaging.server.ServerMessage;

import java.util.ArrayList;
import java.util.List;

public class CatalogNode implements ServerSerializable {

    private boolean visible;
    private int icon;
    private int id;
    private String name;
    private String caption;

    private String parent;
    private int order;

    private List<CatalogNode> children;

    public CatalogNode(boolean visible, int icon, int id, String name, String caption, String parent, int order) {
        this.visible = visible;
        this.icon = icon;
        this.id = id;
        this.name = name;
        this.caption = caption;
        this.parent = parent;
        this.order = order;

        children = new ArrayList<>();
    }

    @Override
    public void writeObject(ServerMessage message) {
        message.writeBoolean(visible);
        message.writeInt(icon);
        message.writeInt(id);
        message.writeString(name);
        message.writeString(caption);

        message.writeInt(0); // offersCount
        // TODO

        message.writeInt(children.size());
        children.forEach(child -> child.writeObject(message));
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<CatalogNode> getChildren() {
        return children;
    }

    public void setChildren(List<CatalogNode> children) {
        this.children = children;
    }
}
