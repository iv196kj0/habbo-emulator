package com.habbo.emulator.habbohotel.catalog;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.server.ServerSerializable;

import java.util.ArrayList;
import java.util.List;

public class CatalogPage implements ServerSerializable {

    private final int id;
    private final String layout;

    private List<CatalogItem> items;

    public CatalogPage(int id, String layout) {
        this.id = id;
        this.layout = layout;

        items = new ArrayList<>();
    }

    @Override
    public void writeObject(ServerMessage message) {
        message.writeInt(id);
        message.writeString("NORMAL");
        message.writeString(layout);

        message.writeInt(3);
        message.writeString("catalog_dividers_header_dyn");
        message.writeString("catalog_dividers_teaser2_001");
        message.writeString("");
        message.writeInt(3);
        message.writeString("Divide and conquer! A well-placed divider can really tie your room together. Take a look below to see what's on offer. ");
        message.writeString("");
        message.writeString("Which one to choose? I'm soooo divided! ");

        message.writeInt(items.size());
        for (CatalogItem item : items) {
            message.writeObject(item);
        }

        message.writeInt(-1); // unknown
        message.writeBoolean(false); // unknown
    }

    public int getId() {
        return id;
    }

    public String getLayout() {
        return layout;
    }

    public List<CatalogItem> getItems() {
        return items;
    }

    public void setItems(List<CatalogItem> items) {
        this.items = items;
    }
}
