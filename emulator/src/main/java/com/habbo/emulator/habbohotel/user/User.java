package com.habbo.emulator.habbohotel.user;

public class User {

    private final int id;
    private String username;
    private String figure;
    private String gender;

    public User(int id, String username, String figure, String gender) {
        this.id = id;
        this.username = username;
        this.figure = figure;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFigure() {
        return figure;
    }

    public void setFigure(String figure) {
        this.figure = figure;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
