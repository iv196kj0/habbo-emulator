package com.habbo.emulator.habbohotel.user;

import com.habbo.emulator.persistence.DAOFactory;
import com.habbo.emulator.persistence.skeleton.user.UserDAO;
import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class UserFactory {

    private static UserFactory instance;
    private final UserDAO dao;

    private final Map<String, User> activeUsers;

    private UserFactory(UserDAO dao) {
        this.dao = dao;
        activeUsers = new HashMap<>();
    }

    public static UserFactory getInstance() {
        if (instance == null) {
            instance = new UserFactory(DAOFactory.getDAOFactory().getUserDAO());
        }
        return instance;
    }

    public User getUserById(int id) {
        return dao.findById(id);
    }

    public User getActiveUser(Channel channel) {
        return activeUsers.get(channel.id().asLongText());
    }

    public void addActiveUser(Channel channel, User user) {
        synchronized (activeUsers) {
            activeUsers.put(channel.id().asLongText(), user);
        }
    }

    public void removeActiveUser(Channel channel) {
        synchronized (activeUsers) {
            activeUsers.remove(channel.id().asLongText());
        }
    }
}
