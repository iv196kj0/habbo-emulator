package com.habbo.emulator.messaging;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.client.handshake.*;
import com.habbo.emulator.messaging.client.hotelview.*;
import com.habbo.emulator.messaging.client.user.*;
import com.habbo.emulator.messaging.client.catalog.*;
import com.habbo.emulator.messaging.types.Request;
import com.habbo.emulator.util.logging.Logger;
import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class PacketHandler {

    private static PacketHandler instance;

    private final Map<Integer, ClientRequest> handlers;

    private PacketHandler() {
        handlers = new HashMap<>();

        registerHandshake();
        registerHotelview();
        registerUser();
        registerCatalog();

        Logger.logHealth(String.format("PacketHandler registered %d handler(s).", handlers.size()));
    }

    public static PacketHandler getInstance() {
        if (instance == null) {
            instance = new PacketHandler();
        }
        return instance;
    }

    private void registerHandshake() {
        handlers.put(Request.RELEASE_VERSION, new ReleaseVersionRequest());
        handlers.put(Request.SECURE_LOGIN, new SecureLoginRequest());
    }

    private void registerHotelview() {
        handlers.put(Request.HOTEL_VIEW_DATA, new HotelViewDataRequest());
        handlers.put(Request.HOTEL_VIEW_REQUEST_BONUS_RARE, new HotelViewRequestBonusRareRequest());
    }

    private void registerUser() {
        handlers.put(Request.REQUEST_USER_DATA, new RequestUserDataRequest());
        handlers.put(Request.USER_SAVE_LOOK, new UserSaveLookRequest());
        handlers.put(Request.USER_ACTIVITY, new UserActivityRequest());
        handlers.put(Request.USER_CREDITS, new UserCreditsRequest());
    }

    private void registerCatalog() {
        handlers.put(Request.REQUEST_CATALOG_MODE, new RequestCatalogModeRequest());
        handlers.put(Request.CATALOG_PAGE, new CatalogPageRequest());
    }

    public void handle(Channel channel, ClientMessage message) {
        ClientRequest handler = handlers.get(message.getHeader());

        if (handler != null) {
            Logger.logPackets(String.format("Handling packet with header: %d", message.getHeader()));
            handler.handle(channel, message);
        } else {
            Logger.logPackets(String.format("[!]No handler found for header: %d", message.getHeader()));
        }
    }
}
