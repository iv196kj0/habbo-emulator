package com.habbo.emulator.messaging.client;

import io.netty.buffer.ByteBuf;

public class ClientMessage {

    private final int header;
    private final ByteBuf buffer;

    public ClientMessage(ByteBuf buffer) {
        this.header = buffer.readShort();
        this.buffer = buffer;
    }

    public int readByte() {
        return buffer.readByte();
    }

    public int readShort() {
        return buffer.readShort();
    }

    public int readInt() {
        return buffer.readInt();
    }

    public String readString() {
        return new String(readBytes(readShort()));
    }

    public int getHeader() {
        return header;
    }

    public byte[] readBytes(int amount) {
        byte[] result = new byte[amount];
        for (int i = 0; i < amount; i++) {
            result[i] = buffer.readByte();
        }
        return result;
    }
}
