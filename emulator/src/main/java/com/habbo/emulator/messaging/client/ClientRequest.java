package com.habbo.emulator.messaging.client;

import io.netty.channel.Channel;

public interface ClientRequest {
    void handle(Channel channel, ClientMessage message);
}
