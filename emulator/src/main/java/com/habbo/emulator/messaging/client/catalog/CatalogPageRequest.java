package com.habbo.emulator.messaging.client.catalog;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.catalog.CatalogPageResponse;
import com.habbo.emulator.messaging.server.catalog.CatalogPagesListResponse;
import io.netty.channel.Channel;

public class CatalogPageRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        final int pageId = message.readInt();
        message.readInt(); // unknown
        final String catalogType = message.readString();

        if (!catalogType.equals("NORMAL")) {
            return;
        }

        channel.writeAndFlush(new CatalogPageResponse(pageId));
    }
}
