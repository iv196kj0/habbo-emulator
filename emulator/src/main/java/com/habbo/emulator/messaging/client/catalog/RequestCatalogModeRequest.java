package com.habbo.emulator.messaging.client.catalog;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.catalog.CatalogPagesListResponse;
import io.netty.channel.Channel;

public class RequestCatalogModeRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        // TODO: verify request-response mapping.
        channel.writeAndFlush(new CatalogPagesListResponse());
    }
}
