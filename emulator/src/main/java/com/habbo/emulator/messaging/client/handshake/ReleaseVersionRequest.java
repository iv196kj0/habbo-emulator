package com.habbo.emulator.messaging.client.handshake;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import io.netty.channel.Channel;

public class ReleaseVersionRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        final String version = message.readString();
        if (!version.equals("PRODUCTION-201712271205-873924611")) {
            return;
        }

        final String applet = message.readString();
        if (!applet.equals("FLASH")) {
            return;
        }

        message.readInt(); // unknown
        message.readInt(); // unknown
    }
}
