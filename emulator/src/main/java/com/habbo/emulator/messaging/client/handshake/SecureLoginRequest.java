package com.habbo.emulator.messaging.client.handshake;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.habbohotel.user.UserFactory;
import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.handshake.*;
import io.netty.channel.Channel;

public class SecureLoginRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        final String sso = message.readString();
        message.readInt(); // unknown

        if (!sso.equals("sso-ticket-here")) {
            return;
        }

        User user = UserFactory.getInstance().getUserById(11333444);
        if (user == null) {
            return;
        }

        UserFactory.getInstance().addActiveUser(channel, user);

        channel.writeAndFlush(new SecureLoginOKResponse());
        channel.writeAndFlush(new UserEffectsListResponse());
        channel.writeAndFlush(new AddHabboItemResponse());
        channel.writeAndFlush(new UserClothesResponse());
        channel.writeAndFlush(new UserHomeRoomResponse());
        channel.writeAndFlush(new FavoriteRoomsCountResponse());
        channel.writeAndFlush(new NewUserIdentityResponse());
        channel.writeAndFlush(new UserPermissionsResponse());
        channel.writeAndFlush(new SessionRightsResponse());
        channel.writeAndFlush(new DebugConsoleResponse());
        channel.writeAndFlush(new UserCurrencyResponse());
        channel.writeAndFlush(new UserAchievementScoreResponse());
        channel.writeAndFlush(new U55129Response());
        channel.writeAndFlush(new U5284eResponse());
        channel.writeAndFlush(new BuildersClubExpiredResponse());
        channel.writeAndFlush(new CfhTopicsMessageResponse());
    }
}
