package com.habbo.emulator.messaging.client.hotelview;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.hotelview.HotelViewDataResponse;
import com.habbo.emulator.util.logging.Logger;
import io.netty.channel.Channel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HotelViewDataRequest implements ClientRequest {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public void handle(Channel channel, ClientMessage message) {

        final String conf = message.readString();
        final String layout = getLayout(conf, new Date());

        channel.writeAndFlush(new HotelViewDataResponse(conf, layout));
    }

    protected String getLayout(String conf, Date now) {
        String layout = "";

        for (String widget : conf.split(";")) {
            if (widget.length() == 0) {
                continue;
            }

            String[] widgetData = widget.split(",");

            if (!hasStarted(widgetData[0], now)) {
                break;
            }

            if (widgetData.length == 1) {
                layout = "";
                continue;
            }

            layout = widgetData[1];
        }

        return layout;
    }

    private boolean hasStarted(String startingDate, Date now) {
        boolean started = false;
        try {
            started = dateFormat.parse(startingDate).before(now);
        } catch (ParseException e) {
            Logger.logException(e);
        }
        return started;
    }
}
