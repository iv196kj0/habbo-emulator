package com.habbo.emulator.messaging.client.hotelview;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.hotelview.BonusRareResponse;
import io.netty.channel.Channel;

public class HotelViewRequestBonusRareRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        channel.writeAndFlush(new BonusRareResponse());
    }
}
