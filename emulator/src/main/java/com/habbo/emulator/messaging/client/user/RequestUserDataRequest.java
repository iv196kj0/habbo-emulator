package com.habbo.emulator.messaging.client.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.habbohotel.user.UserFactory;
import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.hotelview.PerkAllowancesResponse;
import com.habbo.emulator.messaging.server.user.UserDataResponse;
import io.netty.channel.Channel;

public class RequestUserDataRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        User user = UserFactory.getInstance().getActiveUser(channel);
        channel.writeAndFlush(new UserDataResponse(user));
        //AchievementProgress ACH_RegistrationDuration2
        //AchievementProgress ACH_DaysElapsed1
        //AchievementProgress ACH_BuildersClub1
        channel.writeAndFlush(new PerkAllowancesResponse());
    }
}
