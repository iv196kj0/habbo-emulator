package com.habbo.emulator.messaging.client.user;

import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import io.netty.channel.Channel;

public class UserActivityRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        message.readString(); // unknown
        message.readString(); // unknown
        message.readString(); // unknown
        message.readString(); // unknown
        message.readInt(); // unknown
    }
}
