package com.habbo.emulator.messaging.client.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.habbohotel.user.UserFactory;
import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.user.UserCreditsResponse;
import io.netty.channel.Channel;

public class UserCreditsRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        User user = UserFactory.getInstance().getActiveUser(channel);
        channel.writeAndFlush(new UserCreditsResponse(user));
    }
}
