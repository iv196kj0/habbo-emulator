package com.habbo.emulator.messaging.client.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.habbohotel.user.UserFactory;
import com.habbo.emulator.messaging.client.ClientMessage;
import com.habbo.emulator.messaging.client.ClientRequest;
import com.habbo.emulator.messaging.server.user.UpdateUserLook;
import com.habbo.emulator.util.logging.Logger;
import com.habbo.emulator.util.validator.figure.FigureValidator;
import io.netty.channel.Channel;

public class UserSaveLookRequest implements ClientRequest {

    @Override
    public void handle(Channel channel, ClientMessage message) {

        final String gender = message.readString();
        final String figure = message.readString();

        if (!FigureValidator.getInstance().validate(gender, figure)) {
            Logger.logException(String.format("Invalid look: gender: '%s', figure: '%s'", gender, figure));
            return;
        }

        User user = UserFactory.getInstance().getActiveUser(channel);

        user.setGender(gender);
        user.setFigure(figure);

        channel.writeAndFlush(new UpdateUserLook(user));
    }
}
