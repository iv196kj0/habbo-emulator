package com.habbo.emulator.messaging.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class ServerMessage {

    private final ByteBuf buffer;

    public ServerMessage(int header) {
        this.buffer = Unpooled.buffer();
        buffer.writeShort(header);
    }

    public void writeShort(int value) {
        buffer.writeShort(value);
    }

    public void writeInt(int value) {
        buffer.writeInt(value);
    }

    public void writeString(String value) {
        buffer.writeShort(value.length());
        buffer.writeBytes(value.getBytes());
    }

    public void writeBoolean(boolean value) {
        buffer.writeBoolean(value);
    }

    public void writeBytes(byte[] value) {
        buffer.writeBytes(value);
    }

    public void writeObject(ServerSerializable object) {
        object.writeObject(this);
    }

    public ByteBuf getBuffer() {
        return buffer;
    }
}
