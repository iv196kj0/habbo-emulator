package com.habbo.emulator.messaging.server;

public interface ServerSerializable {
    void writeObject(ServerMessage message);
}
