package com.habbo.emulator.messaging.server.catalog;

import com.habbo.emulator.habbohotel.catalog.CatalogFactory;
import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class CatalogPageResponse extends ServerMessage {

    public CatalogPageResponse(int id) {
        super(Response.CATALOG_PAGE);
        super.writeObject(CatalogFactory.getInstance().getCatalogPage(id));
    }
}
