package com.habbo.emulator.messaging.server.catalog;

import com.habbo.emulator.habbohotel.catalog.CatalogFactory;
import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class CatalogPagesListResponse extends ServerMessage {

    public CatalogPagesListResponse() {
        super(Response.CATALOG_PAGES_LIST);
        super.writeObject(CatalogFactory.getInstance().getRootNode());
        super.writeBytes(new byte[] { 0 }); // unknown
        super.writeString("NORMAL"); // unknown
    }
}
