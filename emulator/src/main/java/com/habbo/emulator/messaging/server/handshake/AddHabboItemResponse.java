package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class AddHabboItemResponse extends ServerMessage {

    public AddHabboItemResponse() {
        super(Response.ADD_HABBO_ITEM);
        super.writeBytes(new byte[] { 0, 0, 0, 0 }); // unknown
    }
}
