package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class BuildersClubExpiredResponse extends ServerMessage {

    public BuildersClubExpiredResponse() {
        super(Response.BUILDERS_CLUB_EXPIRED);
        super.writeBytes(new byte[] { 0, 0, 0, 0, 0, 0, 0, 50, 0, 1, (byte)134, 32, 0, 0, 0, 0 }); // unknown
    }
}
