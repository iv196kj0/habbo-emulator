package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class CfhTopicsMessageResponse extends ServerMessage {

    public CfhTopicsMessageResponse() {
        super(Response.CFH_TOPICS_MESSAGE);

        super.writeInt(6); // amount categories

        super.writeString("sexual_content"); // category 1
        super.writeInt(4); // amount category 1

        super.writeString("explicit_sexual_talk");
        super.writeInt(1);
        super.writeString("auto_reply");

        super.writeString("cybersex");
        super.writeInt(2);
        super.writeString("auto_reply");

        super.writeString("sexual_webcam_images");
        super.writeInt(3);
        super.writeString("auto_reply");

        super.writeString("sex_links");
        super.writeInt(36);
        super.writeString("auto_reply");


        super.writeString("pii_meeting_irl"); // category 2
        super.writeInt(2); // amount category 2

        super.writeString("meet_irl");
        super.writeInt(6);
        super.writeString("auto_reply");

        super.writeString("asking_pii");
        super.writeInt(8);
        super.writeString("auto_reply");


        super.writeString("scamming"); // category 3
        super.writeInt(5); // amount category 3

        super.writeString("scamsites_promoting");
        super.writeInt(9);
        super.writeString("auto_reply");

        super.writeString("selling_buying_accounts_or_furni");
        super.writeInt(10);
        super.writeString("auto_reply");

        super.writeString("stealing_accounts_or_furni");
        super.writeInt(11);
        super.writeString("auto_reply");

        super.writeString("hacking_scamming_tricks");
        super.writeInt(32);
        super.writeString("auto_reply");

        super.writeString("fraud");
        super.writeInt(33);
        super.writeString("auto_reply");


        super.writeString("trolling_bad_behavior"); // category 4
        super.writeInt(8); // amount category 4

        super.writeString("bullying");
        super.writeInt(12);
        super.writeString("auto_reply");

        super.writeString("habbo_name");
        super.writeInt(13);
        super.writeString("auto_reply");

        super.writeString("inappropiate_room_group_event");
        super.writeInt(22);
        super.writeString("auto_reply");

        super.writeString("swearing");
        super.writeInt(14);
        super.writeString("auto_reply");

        super.writeString("drugs_promotion");
        super.writeInt(15);
        super.writeString("auto_reply");

        super.writeString("gambling");
        super.writeInt(16);
        super.writeString("auto_reply");

        super.writeString("staff_impersonation");
        super.writeInt(17);
        super.writeString("auto_reply");

        super.writeString("minors_access");
        super.writeInt(18);
        super.writeString("auto_reply");


        super.writeString("violent_behavior"); // category 5
        super.writeInt(3); // amount category 5

        super.writeString("hate_speech");
        super.writeInt(19);
        super.writeString("auto_reply");

        super.writeString("violent_roleplay");
        super.writeInt(20);
        super.writeString("auto_reply");

        super.writeString("self_threatening");
        super.writeInt(21);
        super.writeString("warn");


        super.writeString("game_interruption"); // category 6
        super.writeInt(4); // amount category 6

        super.writeString("flooding");
        super.writeInt(22);
        super.writeString("auto_reply");

        super.writeString("door_blocking");
        super.writeInt(23);
        super.writeString("auto_reply");

        super.writeString("raids");
        super.writeInt(29);
        super.writeString("auto_reply");

        super.writeString("scripting");
        super.writeInt(35);
        super.writeString("auto_reply");
    }
}
