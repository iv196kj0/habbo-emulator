package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class DebugConsoleResponse extends ServerMessage {

    public DebugConsoleResponse() {
        super(Response.DEBUG_CONSOLE);
        super.writeBoolean(true); // unknown
    }
}
