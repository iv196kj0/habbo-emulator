package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class FavoriteRoomsCountResponse extends ServerMessage {

    public FavoriteRoomsCountResponse() {
        super(Response.FAVORITE_ROOMS_COUNT);
        super.writeBytes(new byte[] { 0, 0, 0, 30, 0, 0, 0, 0 }); // unknown
    }
}
