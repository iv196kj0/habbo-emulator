package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class NewUserIdentityResponse extends ServerMessage {

    public NewUserIdentityResponse() {
        super(Response.NEW_USER_IDENTITY);
        super.writeInt(2); // unknown
    }
}
