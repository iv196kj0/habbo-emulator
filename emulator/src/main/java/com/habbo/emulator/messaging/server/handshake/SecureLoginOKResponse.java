package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class SecureLoginOKResponse extends ServerMessage {

    public SecureLoginOKResponse() {
        super(Response.SECURE_LOGIN_OK);
    }
}
