package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class SessionRightsResponse extends ServerMessage {

    public SessionRightsResponse() {
        super(Response.SESSION_RIGHTS);
        super.writeBytes(new byte[] { 1, 0, 1 }); // unknown
    }
}
