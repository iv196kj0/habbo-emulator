package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class U5284eResponse extends ServerMessage {

    public U5284eResponse() {
        super(Response._5284e2c5e5adc1e935ae5f9090c87a9f);
        super.writeString(""); // unknown
        super.writeString(""); // unknown
    }
}
