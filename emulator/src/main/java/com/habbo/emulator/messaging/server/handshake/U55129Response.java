package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class U55129Response extends ServerMessage {

    public U55129Response() {
        super(Response._5512983879202e45959c6557bef8a8ef);
        super.writeBoolean(false); // unknown
    }
}
