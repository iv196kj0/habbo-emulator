package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserAchievementScoreResponse extends ServerMessage {

    public UserAchievementScoreResponse() {
        super(Response.USER_ACHIEVEMENT_SCORE);
        super.writeInt(200); // score
    }
}
