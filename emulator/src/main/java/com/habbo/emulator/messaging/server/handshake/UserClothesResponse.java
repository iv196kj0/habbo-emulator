package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserClothesResponse extends ServerMessage {

    public UserClothesResponse() {
        super(Response.USER_CLOTHES);
        super.writeBytes(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // unknown
    }
}
