package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserCurrencyResponse extends ServerMessage {

    public UserCurrencyResponse() {
        super(Response.USER_CURRENCY);

        super.writeInt(11); // unknown length

        super.writeInt(0); // duckets key
        super.writeInt(100); // duckets value

        super.writeInt(1); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(2); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(3); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(4); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(5); // diamonds key
        super.writeInt(100); // diamonds value

        super.writeInt(101); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(102); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(103); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(104); // unknown key
        super.writeInt(0); // unknown value

        super.writeInt(105); // unknown key
        super.writeInt(0); // unknown value
    }
}
