package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserEffectsListResponse extends ServerMessage {

    public UserEffectsListResponse() {
        super(Response.USER_EFFECTS_LIST);
        super.writeBytes(new byte[] { 0, 0, 0, 0 }); // unknown
    }
}
