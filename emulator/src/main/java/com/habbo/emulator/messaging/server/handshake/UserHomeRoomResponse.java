package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserHomeRoomResponse extends ServerMessage {

    public UserHomeRoomResponse() {
        super(Response.USER_HOME_ROOM);
        super.writeInt(0); // room id
        super.writeInt(0); // unknown
    }
}
