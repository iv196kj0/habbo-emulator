package com.habbo.emulator.messaging.server.handshake;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserPermissionsResponse extends ServerMessage {

    public UserPermissionsResponse() {
        super(Response.USER_PERMISSIONS);
        super.writeInt(0); // unknown
        super.writeInt(0); // unknown
        super.writeBoolean(false); // unknown
    }
}
