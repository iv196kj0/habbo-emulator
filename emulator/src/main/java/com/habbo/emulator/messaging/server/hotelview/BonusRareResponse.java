package com.habbo.emulator.messaging.server.hotelview;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class BonusRareResponse extends ServerMessage {

    public BonusRareResponse() {
        super(Response.BONUS_RARE);
        super.writeString("bonusbag17_4");
        super.writeInt(9764); // item id
        super.writeInt(120); // goal credits
        super.writeInt(120); // remaining credits
    }
}
