package com.habbo.emulator.messaging.server.hotelview;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class HotelViewDataResponse extends ServerMessage {

    public HotelViewDataResponse(String conf, String layout) {
        super(Response.HOTEL_VIEW_DATA);
        super.writeString(conf);
        super.writeString(layout);
    }
}
