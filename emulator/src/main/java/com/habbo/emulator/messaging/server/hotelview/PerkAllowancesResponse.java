package com.habbo.emulator.messaging.server.hotelview;

import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class PerkAllowancesResponse extends ServerMessage {

    public PerkAllowancesResponse() {
        super(Response.PERK_ALLOWANCES);

        super.writeInt(12); // count

        super.writeString("BUILDER_AT_WORK");
        super.writeString("requirement.unfulfilled.group_membership");
        super.writeBoolean(false);

        super.writeString("MOUSE_ZOOM");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("HABBO_CLUB_OFFER_BETA");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("JUDGE_CHAT_REVIEWS");
        super.writeString("requirement.unfulfilled.helper_level_6");
        super.writeBoolean(false);

        super.writeString("USE_GUIDE_TOOL");
        super.writeString("requirement.unfulfilled.helper_level_4");
        super.writeBoolean(false);

        super.writeString("TRADE");
        super.writeString("requirement.unfulfilled.citizenship_level_3");
        super.writeBoolean(false);

        super.writeString("NAVIGATOR_ROOM_THUMBNAIL_CAMERA");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("NAVIGATOR_PHASE_TWO_2014");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("CAMERA");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("CALL_ON_HELPERS");
        super.writeString("");
        super.writeBoolean(true);

        super.writeString("CITIZEN");
        super.writeString("requirement.unfulfilled.citizenship_level_4");
        super.writeBoolean(false);

        super.writeString("VOTE_IN_COMPETITIONS");
        super.writeString("requirement.unfulfilled.helper_level_2");
        super.writeBoolean(false);
    }
}
