package com.habbo.emulator.messaging.server.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UpdateUserLook extends ServerMessage {

    public UpdateUserLook(User user) {
        super(Response.UPDATE_USER_LOOK);
        super.writeString(user.getFigure());
        super.writeString(user.getGender());
    }
}
