package com.habbo.emulator.messaging.server.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserCreditsResponse extends ServerMessage {

    public UserCreditsResponse(User user) {
        super(Response.USER_CREDITS);
        super.writeString("100.0"); // credits
    }
}
