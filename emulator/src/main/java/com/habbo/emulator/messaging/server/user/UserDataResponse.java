package com.habbo.emulator.messaging.server.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.messaging.server.ServerMessage;
import com.habbo.emulator.messaging.types.Response;

public class UserDataResponse extends ServerMessage {

    public UserDataResponse(User user) {
        super(Response.USER_DATA);
        super.writeInt(user.getId());
        super.writeString(user.getUsername());
        super.writeString(user.getFigure());
        super.writeString(user.getGender());
        super.writeString(""); // unknown
        super.writeString(""); // unknown
        super.writeBoolean(false); // unknown
        super.writeInt(0); // unknown
        super.writeInt(3); // unknown
        super.writeInt(3); // unknown
        super.writeBoolean(true); // unknown
        super.writeString("12-12-2012 12:12:12"); // unknown
        super.writeBoolean(true); // unknown
        super.writeBoolean(false); // unknown
    }
}
