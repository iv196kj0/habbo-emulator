package com.habbo.emulator.messaging.types;

// PRODUCTION-201712271205-873924611
public final class Request {
    // handshake
    public static final int RELEASE_VERSION = 4000; // 44163599b7cc457cf5adcee17957b1e8
    public static final int SECURE_LOGIN = 1804; // f190cdc97eea13b1c4e37e4ca38c011b
    // hotelview
    public static final int HOTEL_VIEW_DATA = 1959; // 1dc3a8f0dee1dece50ac062e3f5c9b3b
    public static final int HOTEL_VIEW_REQUEST_BONUS_RARE = 356; // d0007d2cad988b927eb1e855a5a6390f
    // user
    public static final int REQUEST_USER_DATA = 1356; // 176fb2a1d3f3878b9b125e145756f57c
    public static final int USER_SAVE_LOOK = 498; // 1f03ea2553d1f3a013c2d9aa30afaa8a
    public static final int USER_ACTIVITY = 3478; // ba6e5ec5767486804f64557233e71a03
    public static final int USER_CREDITS = 920; // 1b8ddfa328525f832da018d48301f871
    // catalog
    public static final int REQUEST_CATALOG_MODE = 194; // dada1900b580b6935129bb394c835c3a
    public static final int CATALOG_PAGE = 2555; // 581574d1aae8a2da37cb4b3422f993bd
}
