package com.habbo.emulator.messaging.types;

// PRODUCTION-201712271205-873924611
public final class Response {
    // handshake
    public static final int SECURE_LOGIN_OK = 116; // 0a545d2386ea76176ecbcdf7aedf5d04
    public static final int USER_EFFECTS_LIST = 694; // b5b15b2834623a7a4d8e7e425b0cf018
    public static final int ADD_HABBO_ITEM = 3707; // b1f5149edc67ded09c6f00b372fbfdb3
    public static final int USER_CLOTHES = 2740; // 977c3ed3f7a212857877a1d5ece47e6a
    public static final int USER_HOME_ROOM = 2891; // c97618e1782ac353fedaf6d4a64c4cb2
    public static final int FAVORITE_ROOMS_COUNT = 430; // 6bfda1136290d33976841db4a4275e7d
    public static final int NEW_USER_IDENTITY = 2797; // 14b70bf11ec5f95aecde1529dc94c3ff
    public static final int USER_PERMISSIONS = 637; // dc879502dd1158c61acdde61433a9bd7
    public static final int SESSION_RIGHTS = 1586; // 0cc3af4db3999a628e8720d93191347d
    public static final int DEBUG_CONSOLE = 1460; // 4eaebff189e10b5573774e5bfab45498
    public static final int USER_CURRENCY = 205; // f35b3ca9d7c9a364d1b35f81193e157a
    public static final int USER_ACHIEVEMENT_SCORE = 1042; // 30ade5f82ffa063dfcb65a2b75e47716
    public static final int _5512983879202e45959c6557bef8a8ef = 1302; // 5512983879202e45959c6557bef8a8ef
    public static final int _5284e2c5e5adc1e935ae5f9090c87a9f = 1635; // 5284e2c5e5adc1e935ae5f9090c87a9f
    public static final int BUILDERS_CLUB_EXPIRED = 548; // 01d1ee9a21596d5e8d64808d92e3db3c
    public static final int CFH_TOPICS_MESSAGE = 354; // 66e72a1e2c7e44706ad70ffc3edf2b06
    public static final int PING = 1612; // 5066be53f1b24abd1cf48a5b11237afa
    // hotelview
    public static final int HOTEL_VIEW_DATA = 2336; // f36155cc83a8b7443463b74144af3695
    public static final int PERK_ALLOWANCES = 3446; // 6a803fa68f0ee24808a0c17fd417e592
    public static final int BONUS_RARE = 838; // 781d784034e0120cfadf790b8925a826
    // user
    public static final int USER_DATA = 154; // 94c7f17cb47d49281b6ee2e19b72b018
    public static final int UPDATE_USER_LOOK = 3673; // 92b8b073252df23a7b4e8b8b21759221
    public static final int USER_CREDITS = 2277; // 494106ca0e88d0e11d7263a390c4dbfb
    // catalog
    public static final int CATALOG_PAGES_LIST = 834; // 599c6043cc855bbeecebb2ead7c7a6f9
    public static final int CATALOG_PAGE = 1561; // 8cd57bb7ca6f920991554ea36ab615cf
}
