package com.habbo.emulator.networking.pipeline;

import com.habbo.emulator.habbohotel.user.UserFactory;
import com.habbo.emulator.messaging.PacketHandler;
import com.habbo.emulator.messaging.client.ClientMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

public class InboundHandler extends SimpleChannelInboundHandler<ClientMessage> {

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        //
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        UserFactory.getInstance().removeActiveUser(ctx.channel());
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, ClientMessage msg) {
        try {
            PacketHandler.getInstance().handle(ctx.channel(), msg);
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
}
