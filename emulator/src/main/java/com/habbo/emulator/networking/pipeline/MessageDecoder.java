package com.habbo.emulator.networking.pipeline;

import com.habbo.emulator.messaging.client.ClientMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;

public class MessageDecoder extends ByteToMessageDecoder {

    private static final String CROSS_DOMAIN_POLICY =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n" +
                    "<cross-domain-policy xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.adobe.com/xml/schemas/PolicyFileSocket.xsd\"> \n" +
                    "\t<allow-access-from domain=\"*\" to-ports=\"*\"/> \n" +
                    "</cross-domain-policy>\0";

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        if (in.readableBytes() < 6) {
            return;
        }

        in.markReaderIndex();

        int length = in.readInt();

        if (length == 1014001516) {
            ctx.channel().writeAndFlush(Unpooled.copiedBuffer(CROSS_DOMAIN_POLICY, CharsetUtil.UTF_8));
            in.readBytes(19);
            return;
        }

        if (in.readableBytes() < length) {
            in.resetReaderIndex();
            return;
        }

        out.add(new ClientMessage(in.readBytes(length)));
    }
}
