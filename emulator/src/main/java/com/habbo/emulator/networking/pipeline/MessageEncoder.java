package com.habbo.emulator.networking.pipeline;

import com.habbo.emulator.messaging.server.ServerMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

public class MessageEncoder extends MessageToMessageEncoder<ServerMessage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, ServerMessage msg, List<Object> out) throws Exception {

        byte[] data = msg.getBuffer().array();

        ByteBuf buffer = Unpooled.buffer(data.length + 4);
        buffer.writeInt(data.length);
        buffer.writeBytes(data);

        out.add(buffer);
    }
}
