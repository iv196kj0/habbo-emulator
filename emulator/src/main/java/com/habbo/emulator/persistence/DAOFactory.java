package com.habbo.emulator.persistence;

import com.habbo.emulator.persistence.postgres.PostgresDAOFactory;
import com.habbo.emulator.persistence.skeleton.catalog.CatalogNodeDAO;
import com.habbo.emulator.persistence.skeleton.user.UserDAO;

public abstract class DAOFactory {

    public abstract UserDAO getUserDAO();
    public abstract CatalogNodeDAO getCatalogNodeDAO();

    public static DAOFactory getDAOFactory() {
        return new PostgresDAOFactory();
    }
}
