package com.habbo.emulator.persistence.postgres;

import com.habbo.emulator.persistence.DAOFactory;
import com.habbo.emulator.persistence.postgres.catalog.PostgresCatalogNodeDAO;
import com.habbo.emulator.persistence.postgres.user.PostgresUserDAO;
import com.habbo.emulator.persistence.skeleton.catalog.CatalogNodeDAO;
import com.habbo.emulator.persistence.skeleton.user.UserDAO;
import com.habbo.emulator.util.logging.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PostgresDAOFactory extends DAOFactory {

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String DBURL = "jdbc:postgresql://localhost:5432/habbo";

    public PostgresDAOFactory() {
        //
    }

    private static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                DBURL, "postgres", "postgres"
        );
    }

    public static Connection getConnection() throws SQLException {
        return createConnection();
    }

    public static void cleanup(PreparedStatement statement, Connection connection) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                Logger.logException(e);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                Logger.logException(e);
            }
        }
    }

    @Override
    public UserDAO getUserDAO() {
        return new PostgresUserDAO();
    }

    @Override
    public CatalogNodeDAO getCatalogNodeDAO() {
        return new PostgresCatalogNodeDAO();
    }
}
