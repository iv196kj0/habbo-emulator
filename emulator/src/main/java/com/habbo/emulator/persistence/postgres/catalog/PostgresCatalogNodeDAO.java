package com.habbo.emulator.persistence.postgres.catalog;

import com.habbo.emulator.habbohotel.catalog.CatalogNode;
import com.habbo.emulator.persistence.postgres.PostgresDAOFactory;
import com.habbo.emulator.persistence.skeleton.catalog.CatalogNodeDAO;
import com.habbo.emulator.util.logging.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostgresCatalogNodeDAO implements CatalogNodeDAO {

    @Override
    public CatalogNode findByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = PostgresDAOFactory.getConnection();

            int index = 0;
            statement = connection.prepareStatement("SELECT visible, icon, id, name, caption, parent, \"order\" FROM catalog_node WHERE name = ?");
            statement.setString(++index, name);

            return createObjectFromResult(statement.executeQuery());
        } catch (SQLException e) {
            Logger.logException(e);
        } finally {
            PostgresDAOFactory.cleanup(statement, connection);
        }

        return null;
    }

    @Override
    public List<CatalogNode> findByParent(String parent) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = PostgresDAOFactory.getConnection();

            int index = 0;
            statement = connection.prepareStatement("SELECT visible, icon, id, name, caption, parent, \"order\" FROM catalog_node WHERE parent = ?");
            statement.setString(++index, parent);

            return createObjectArrayFromResult(statement.executeQuery());
        } catch (SQLException e) {
            Logger.logException(e);
        } finally {
            PostgresDAOFactory.cleanup(statement, connection);
        }

        return null;
    }

    @Override
    public void insert(CatalogNode node) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = PostgresDAOFactory.getConnection();

            int index = 0;
            statement = connection.prepareStatement("INSERT INTO catalog_node (visible, icon, id, name, caption, parent, \"order\") VALUES (?, ?, ?, ?, ?, ?, ?)");
            statement.setBoolean(++index, node.isVisible());
            statement.setInt(++index, node.getIcon());
            statement.setInt(++index, node.getId());
            statement.setString(++index, node.getName());
            statement.setString(++index, node.getCaption());
            statement.setString(++index, node.getParent());
            statement.setInt(++index, node.getOrder());

            statement.executeUpdate();
        } catch (SQLException e) {
            Logger.logException(e);
        } finally {
            PostgresDAOFactory.cleanup(statement, connection);
        }
    }

    private List<CatalogNode> createObjectArrayFromResult(ResultSet result) throws SQLException {
        List<CatalogNode> nodes = new ArrayList<>();
        while (result.next()) {
            nodes.add(new CatalogNode(
                    result.getBoolean("visible"),
                    result.getInt("icon"),
                    result.getInt("id"),
                    result.getString("name"),
                    result.getString("caption"),

                    result.getString("parent"),
                    result.getInt("order")
            ));
        }
        return nodes;
    }

    private CatalogNode createObjectFromResult(ResultSet result) throws SQLException {
        if (result.next()) {
            return new CatalogNode(
                    result.getBoolean("visible"),
                    result.getInt("icon"),
                    result.getInt("id"),
                    result.getString("name"),
                    result.getString("caption"),

                    result.getString("parent"),
                    result.getInt("order")
            );
        }

        return null;
    }
}
