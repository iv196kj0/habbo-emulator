package com.habbo.emulator.persistence.postgres.user;

import com.habbo.emulator.habbohotel.user.User;
import com.habbo.emulator.persistence.postgres.PostgresDAOFactory;
import com.habbo.emulator.persistence.skeleton.user.UserDAO;
import com.habbo.emulator.util.logging.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostgresUserDAO implements UserDAO {

    @Override
    public User findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = PostgresDAOFactory.getConnection();

            int index = 0;
            statement = connection.prepareStatement("SELECT id, username, figure, gender FROM users WHERE id = ?");
            statement.setInt(++index, id);

            return createObjectFromResult(statement.executeQuery());
        } catch (SQLException e) {
            Logger.logException(e);
        } finally {
            PostgresDAOFactory.cleanup(statement, connection);
        }

        return null;
    }

    private User createObjectFromResult(ResultSet result) throws SQLException {
        if (result.next()) {
            return new User(
                    result.getInt("id"),
                    result.getString("username"),
                    result.getString("figure"),
                    result.getString("gender")
            );
        }

        return null;
    }
}
