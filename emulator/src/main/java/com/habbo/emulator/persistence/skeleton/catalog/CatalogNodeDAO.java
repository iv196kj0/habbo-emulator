package com.habbo.emulator.persistence.skeleton.catalog;

import com.habbo.emulator.habbohotel.catalog.CatalogNode;

import java.util.List;

public interface CatalogNodeDAO {
    CatalogNode findByName(String name);
    List<CatalogNode> findByParent(String parent);
    void insert(CatalogNode node);
}
