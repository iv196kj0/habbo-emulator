package com.habbo.emulator.persistence.skeleton.user;

import com.habbo.emulator.habbohotel.user.User;

public interface UserDAO {
    User findById(int id);
}
