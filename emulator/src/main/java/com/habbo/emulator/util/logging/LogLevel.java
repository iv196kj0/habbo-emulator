package com.habbo.emulator.util.logging;

public enum LogLevel {
    DEVELOPMENT(1),
    PRODUCTION(2);

    private final int level;

    LogLevel(int level) {
        this.level = level;
    }

    public int getValue() {
        return level;
    }
}
