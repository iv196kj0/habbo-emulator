package com.habbo.emulator.util.logging;

public class Logger {

    private static final LogLevel level = LogLevel.DEVELOPMENT;

    public static void logHealth(String message) {
        printMessage(message);
    }

    public static void logPackets(String message) {
        printMessage(message);
    }

    public static void logException(String message) {
        printMessage("[!]" + message);
    }

    public static void logException(Exception e) {
        e.printStackTrace();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }
}
