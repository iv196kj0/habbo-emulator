package com.habbo.emulator.util.validator.figure;

import com.habbo.emulator.util.logging.Logger;
import com.habbo.emulator.util.validator.figure.model.FigureData;
import com.habbo.emulator.util.validator.figure.model.colors.FigureColor;
import com.habbo.emulator.util.validator.figure.model.colors.FigurePalette;
import com.habbo.emulator.util.validator.figure.model.sets.FigureSet;
import com.habbo.emulator.util.validator.figure.model.sets.FigureSetType;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FigureValidator {

    private static FigureValidator instance;

    private FigureData figureData;

    private FigureValidator() {

        BufferedReader reader = null;

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            reader = new BufferedReader(new FileReader(classLoader.getResource("figuredata.xml").getFile()));
        } catch (NullPointerException | FileNotFoundException e) {
            Logger.logHealth("[!]FigureValidator not initialized - no figuredata.xml found.");
            return;
        }

        try {
            reader.readLine();
            figureData = new FiguresXmlParser(reader.readLine()).parse();
        } catch (IOException | NumberFormatException e) {
            Logger.logHealth("[!]FigureValidator not initialized - figuredata.xml invalid format.");
            return;
        }

        Logger.logHealth("FigureValidator initialized.");
    }

    public static FigureValidator getInstance() {
        if (instance == null) {
            instance = new FigureValidator();
        }
        return instance;
    }

    public boolean validate(String gender, String figure) {
        System.out.println(figure);
        if (figureData == null) {
            return true;
        }

        if (!gender.equals("M") && !gender.equals("F")) {
            return false;
        }

        String[] components = figure.split("\\.");
        for (String component : components) {
            String[] typePartColor = component.split("-");

            FigureSetType setType = figureData.getSetType(typePartColor[0]);
            if (setType == null) {
                return false;
            }

            FigureSet set = setType.getSet(typePartColor[1]);
            if (set == null) {
                return false;
            }
            if (!set.getGender().equals(gender) && !set.getGender().equals("U")) {
                return false;
            }

            FigurePalette palette = figureData.getPalette(setType.getPaletteId());
            FigureColor color = palette.getColor(typePartColor[2]);
            if (color == null) {
                return false;
            }
            if (color.getSelectable() == 0) {
                return false;
            }
        }

        return true;
    }

}
