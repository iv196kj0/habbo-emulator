package com.habbo.emulator.util.validator.figure;

import com.habbo.emulator.util.validator.figure.model.FigureData;
import com.habbo.emulator.util.validator.figure.model.colors.FigureColor;
import com.habbo.emulator.util.validator.figure.model.colors.FigurePalette;
import com.habbo.emulator.util.validator.figure.model.sets.FigurePart;
import com.habbo.emulator.util.validator.figure.model.sets.FigureSet;
import com.habbo.emulator.util.validator.figure.model.sets.FigureSetType;

import java.util.ArrayList;
import java.util.List;

public class FiguresXmlParser {

    private final String figuresXml;
    private List<String[]> figuresTags;

    public FiguresXmlParser(String figuresXml) {
        this.figuresXml = figuresXml;
        prepareData();
    }

    private void prepareData() {
        figuresTags = new ArrayList<>();

        String[] tags = figuresXml.split("<");
        for (String tag : tags) {
            if (tag.startsWith("/")) {
                continue;
            }
            tag = tag.split(">")[0];
            tag = tag.split("/")[0];
            figuresTags.add(tag.split(" "));
        }
    }

    public FigureData parse() {
        FigureData result = new FigureData();

        FigurePalette lastPalette = null;
        FigureSetType lastSetType = null;
        FigureSet lastSet = null;

        for (String[] tag : figuresTags) {
            switch (tag[0]) {
                case "palette":
                    FigurePalette palette = new FigurePalette(
                            readInteger(tag, "id")
                    );
                    result.addPalette(palette);
                    lastPalette = palette;
                    break;

                case "color":
                    FigureColor color = new FigureColor(
                            readString(tag, "id"),
                            readInteger(tag, "club"),
                            readInteger(tag, "selectable")
                    );
                    lastPalette.addColor(color);
                    break;

                case "settype":
                    FigureSetType setType = new FigureSetType(
                            readString(tag, "type"),
                            readInteger(tag, "paletteid")
                    );
                    result.addSetType(setType);
                    lastSetType = setType;
                    break;

                case "set":
                    FigureSet set = new FigureSet(
                            readString(tag, "id"),
                            readString(tag, "gender"),
                            readInteger(tag, "club")
                    );
                    lastSetType.addSet(set);
                    lastSet = set;
                    break;

                case "part":
                    FigurePart part = new FigurePart(
                            readString(tag, "id"),
                            readString(tag, "type")
                    );
                    lastSet.addPart(part);
                    break;

                case "hiddenlayers":
                    // unknown
                    break;

                case "layer":
                    // unknown
                    break;
            }
        }

        return result;
    }

    private String readString(String[] tag, String key) {
        String result = null;
        for (String attribute : tag) {
            String[] keyValue = attribute.split("=");
            if (keyValue.length == 1) {
                continue;
            }

            if (keyValue[0].equals(key)) {
                result = keyValue[1].split("\"")[1];
            }
        }
        return result;
    }

    private int readInteger(String[] tag, String key) {
        return Integer.parseInt(readString(tag, key));
    }

}
