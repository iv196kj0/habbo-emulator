package com.habbo.emulator.util.validator.figure.model;

import com.habbo.emulator.util.validator.figure.model.colors.FigurePalette;
import com.habbo.emulator.util.validator.figure.model.sets.FigureSetType;

import java.util.HashMap;
import java.util.Map;

public class FigureData {

    private final Map<Integer, FigurePalette> colors;
    private final Map<String, FigureSetType> sets;

    public FigureData() {
        this.colors = new HashMap<>();
        this.sets = new HashMap<>();
    }

    public FigurePalette getPalette(int id) {
        return colors.get(id);
    }

    public void addPalette(FigurePalette palette) {
        colors.put(palette.getId(), palette);
    }

    public FigureSetType getSetType(String type) {
        return sets.get(type);
    }

    public void addSetType(FigureSetType setType) {
        sets.put(setType.getType(), setType);
    }
}
