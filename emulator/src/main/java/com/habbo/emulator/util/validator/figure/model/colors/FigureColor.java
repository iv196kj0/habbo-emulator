package com.habbo.emulator.util.validator.figure.model.colors;

public class FigureColor {

    private final String id;
    private final int club;
    private final int selectable;

    public FigureColor(String id, int club, int selectable) {
        this.id = id;
        this.club = club;
        this.selectable = selectable;
    }

    public String getId() {
        return id;
    }

    public int getClub() {
        return club;
    }

    public int getSelectable() {
        return selectable;
    }
}
