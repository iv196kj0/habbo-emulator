package com.habbo.emulator.util.validator.figure.model.colors;

import java.util.HashMap;
import java.util.Map;

public class FigurePalette {

    private final int id;

    private final Map<String, FigureColor> children;

    public FigurePalette(int id) {
        this.id = id;

        children = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public void addColor(FigureColor color) {
        children.put(color.getId(), color);
    }

    public FigureColor getColor(String id) {
        return children.get(id);
    }
}
