package com.habbo.emulator.util.validator.figure.model.sets;

public class FigurePart {

    private final String id;
    private final String type;

    public FigurePart(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }
}
