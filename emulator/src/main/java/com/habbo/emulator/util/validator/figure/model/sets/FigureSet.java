package com.habbo.emulator.util.validator.figure.model.sets;

import java.util.ArrayList;
import java.util.List;

public class FigureSet {

    private final String id;
    private final String gender;
    private final int club;

    private final List<FigurePart> children;

    public FigureSet(String id, String gender, int club) {
        this.id = id;
        this.gender = gender;
        this.club = club;

        children = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public int getClub() {
        return club;
    }

    public void addPart(FigurePart part) {
        children.add(part);
    }
}
