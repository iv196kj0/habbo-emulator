package com.habbo.emulator.util.validator.figure.model.sets;

import java.util.HashMap;
import java.util.Map;

public class FigureSetType {

    private final String type;
    private final int paletteId;

    private final Map<String, FigureSet> children;

    public FigureSetType(String type, int paletteId) {
        this.type = type;
        this.paletteId = paletteId;

        children = new HashMap<>();
    }

    public String getType() {
        return type;
    }

    public int getPaletteId() {
        return paletteId;
    }

    public void addSet(FigureSet set) {
        children.put(set.getId(), set);
    }

    public FigureSet getSet(String id) {
        return children.get(id);
    }
}
