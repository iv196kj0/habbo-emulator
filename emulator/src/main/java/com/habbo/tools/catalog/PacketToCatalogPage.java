package com.habbo.tools.catalog;

import com.habbo.tools.util.TanjiReader;

public class PacketToCatalogPage {

    private static final String INPUT = "";

    public static void main(String[] args) {

        TanjiReader reader = new TanjiReader(INPUT);

        int id = reader.readInt();
        String catalogType = reader.readString();
        String layout = reader.readString();

        switch (layout) {

            case "default_3x3":
                System.out.println(reader.readInt());
                System.out.println(reader.readString());
                System.out.println(reader.readString());
                System.out.println(reader.readString());

                System.out.println(reader.readInt());
                System.out.println(reader.readString());
                System.out.println(reader.readString());
                System.out.println(reader.readString());

                int itemsCount = reader.readInt();
                System.out.println(itemsCount);

                for (int i = 0; i < itemsCount; i++) {
                    System.out.println(reader.readInt()); // id
                    System.out.println(reader.readString()); // name
                    System.out.println(reader.readBoolean()); // ?
                    System.out.println(reader.readInt()); // credits
                    System.out.println(reader.readInt()); // points
                    System.out.println(reader.readInt()); // points type
                    System.out.println(reader.readBoolean()); // giftable

                    System.out.println(reader.readInt()); // items count
                    System.out.println(reader.readString()); // item type
                    System.out.println(reader.readInt()); // item sprite
                    System.out.println(reader.readString()); // item extradata

                    System.out.println(reader.readBytes(13)); // unknown
                }

                System.out.println(reader.readInt());
                System.out.println(reader.readBoolean());
                break;

            default:
                System.out.println("unknown layout: " + layout);
                break;
        }
    }
}
