package com.habbo.tools.catalog;

import com.habbo.emulator.habbohotel.catalog.CatalogNode;
import com.habbo.emulator.persistence.DAOFactory;
import com.habbo.tools.util.TanjiReader;

public class PacketToCatalogPagesList {

    private static final String INPUT = "";

    public static void main(String[] args) {

        TanjiReader reader = new TanjiReader(INPUT);

        parseNode(reader, "", 0);

        reader.readByte(); // unknown
        reader.readString(); // NORMAL
    }

    private static void parseNode(TanjiReader reader, String parent, int order) {

        CatalogNode node = new CatalogNode(
                reader.readBoolean(), // visible
                reader.readInt(), // icon
                reader.readInt(), // id
                reader.readString(), // name
                reader.readString(), // caption
                parent, order
        );

        DAOFactory.getDAOFactory().getCatalogNodeDAO().insert(node);

        int offersCount = reader.readInt();
        for (int i = 0; i < offersCount; i++) {
            int offer = reader.readInt();
            // TODO
        }

        int pagesCount = reader.readInt();
        for (int i = 0; i < pagesCount; i++) {
            parseNode(reader, node.getName(), i+1);
        }
    }
}
