package com.habbo.tools.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.util.ArrayList;
import java.util.List;

public class TanjiReader {

    private final ByteBuf buffer;

    public TanjiReader(String buffer) {
        this.buffer = Unpooled.wrappedBuffer(parse(buffer));
    }

    private byte[] parse(String in) {
        while (in.contains("[[")) {
            in = in.replace("[[", "[91][");
        }
        while (in.contains("]]")) {
            in = in.replace("]]", "][93]");
        }
        return parse(in.toCharArray());
    }

    private byte[] parse(char[] in) {
        List<Byte> out = new ArrayList<>();

        for (int i = 0; i < in.length; i++) {

            if (in[i] != '[') {

                int v = (int)in[i];


                if (v > 255) {
                    switch (v) {
                        case 338: v= 140; break;
                        case 339: v=156; break;
                        case 352: v=138; break;
                        case 353: v=154; break;
                        case 376: v=159; break;
                        case 381: v=142; break;
                        case 382: v=158; break;
                        case 401: v=131; break;
                        case 402: v=131; break;
                        case 710: v=136; break;
                        case 732: v=152; break;
                        case 8211: v=150; break;
                        case 8212: v=151; break;
                        case 8216: v=145; break;
                        case 8217: v=146; break;
                        case 8218: v=130; break;
                        case 8220: v=147; break;
                        case 8221: v=148; break;
                        case 8222: v=132; break;
                        case 8224: v=134; break;
                        case 8225: v=135; break;
                        case 8226: v=149; break;
                        case 8230: v=133; break;
                        case 8240: v=137; break;
                        case 8249: v=139; break;
                        case 8250: v=155; break;
                        case 8364: v=128; break;
                        case 8482: v=153; break;
                        case 8776: v=152; break;
                    }
                }

                if (v > 255) {
                    System.out.println("NOOOOO: " + v);
                }


                out.add((byte)v);
                continue;
            }

            int value = 0;
            i++;
            while (in[i] != ']') {
                if (!Character.isDigit(in[i])) {
                    System.out.println("oh oh");
                    System.exit(0);
                }
                int digit = Integer.parseInt(new String(new char[] { in[i] }));
                value *= 10;
                value += digit;

                i++;
            }
            out.add((byte)value);
            //i++;


        }

        return toBytes(out);
    }

    private byte[] toBytes(List<Byte> list) {
        byte[] result = new byte[list.size()];
        for(int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public int readByte() {
        return buffer.readByte();
    }

    public int readShort() {
        return buffer.readShort();
    }

    public int readInt() {
        return (int)buffer.readUnsignedInt();
    }

    public String readString() {
        return new String(readBytes(readShort()));
    }

    public byte[] readBytes(int amount) {
        byte[] result = new byte[amount];
        for (int i = 0; i < amount; i++) {
            result[i] = buffer.readByte();
        }
        return result;
    }

    public boolean readBoolean() {
        return buffer.readBoolean();
    }
}
