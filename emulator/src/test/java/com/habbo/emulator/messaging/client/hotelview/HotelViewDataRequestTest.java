package com.habbo.emulator.messaging.client.hotelview;

import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

public class HotelViewDataRequestTest {

    private DateFormat dateFormat;
    private HotelViewDataRequest request;

    @Before
    public void setUp() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        request = new HotelViewDataRequest();
    }

    @Test
    public void getLayoutTest() throws Exception {
        String testConf;

        testConf = "";
        assertEquals("", request.getLayout(testConf, dateFormat.parse("2015-01-01 12:00")));

        testConf = "2013-05-08 13:00,gamesmaker;2013-05-11 13:00";
        assertEquals("gamesmaker", request.getLayout(testConf, dateFormat.parse("2013-05-09 13:00")));
        assertEquals("", request.getLayout(testConf, dateFormat.parse("2013-05-12 13:00")));

        testConf = "2017-04-14 11:00,xmas16classic";
        assertEquals("xmas16classic", request.getLayout(testConf, dateFormat.parse("2017-12-26 13:00")));

        testConf = "2017-10-05 15:00,cave17treasure;2017-10-23 11:00,cave17cloth;2017-10-25 15:00,cave17knights;2017-10-26 15:00,cave17cloth;2017-10-27 11:00,cave17throne;2017-10-30 15:00,cave17ltd";
        assertEquals("cave17throne", request.getLayout(testConf, dateFormat.parse("2017-10-28 13:00")));
    }

}
