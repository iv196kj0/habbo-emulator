package com.habbo.emulator.util.validator.figure;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FigureValidatorTest {

    private FigureValidator validator;

    @Before
    public void setUp() {
        validator = FigureValidator.getInstance();
    }

    @Test
    public void validateTest() {
        assertTrue(validator.validate("M", "ea-1404-64.he-1610-64.ch-878-71-80.lg-280-81.hr-170-34.hd-207-1370"));
        assertTrue(validator.validate("F", "ha-1015-68.hd-610-1369.ch-635-66.lg-3116-73-82.sh-735-75"));
    }
}
