DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id integer PRIMARY KEY,
    username character varying,
    figure character varying,
    gender character(1)
);

INSERT INTO users (id, username, figure, gender)
    VALUES (11333444, 'mockingbird', 'hr-891-34.hd-209-10.ch-255-71.lg-280-81', 'M');

DROP TABLE IF EXISTS catalog_node;
CREATE TABLE catalog_node (
    visible boolean,
    icon integer,
    id integer,
    name character varying PRIMARY KEY,
    caption character varying,
    parent character varying,
    "order" integer
);

INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 0, -1, 'root', '', '', 0);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 213, 763521, 'front_page', ' Front Page', 'root', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 2, -1, 'furni', 'Furni', 'root', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 146, 763524, 'credit_exchange', 'Credit Furni', 'furni', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, -1, 'room_bundles', 'Room Bundles', 'furni', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 268, 763561, 'xmas17rink', 'Ice Rink Bundle', 'room_bundles', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 268, 763562, 'xmas17carol', 'Carol Singer Bundle', 'room_bundles', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763563, 'xmas15baby1', 'Kitten''s Cosy Kitchen Habitat Bundle', 'room_bundles', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763564, 'xmas15baby2', 'Piglet Pen Habitat Bundle', 'room_bundles', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763565, 'xmas15baby3', 'Polar Bear Habitat Bundle', 'room_bundles', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763566, 'xmas15baby4', 'Puppy''s Paradise Habitat Bundle', 'room_bundles', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763567, 'xmas15baby5', 'Terrier in a Tower Habitat Bundle', 'room_bundles', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 55, 763569, 'room_ad', 'Room Event', 'furni', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 42, 763570, 'xmas17goldhat', 'RARE Gold Top Hat', 'furni', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 42, 763571, 'xmas17goldflow', 'RARE Gold Feather Pin', 'furni', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 277, 763572, 'xmas17furni', 'Victorian Christmas', 'furni', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 263, 763573, 'xmas17crafting', 'Victorian Crafting', 'furni', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 263, 763574, 'xmas17crafting2', 'Victorian Ingredients', 'xmas17crafting', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 79, 763575, 'xmas17dolls', 'Collectible Victorian Dolls', 'furni', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 90, 763577, 'xmas16old', 'Xmas Comeback', 'furni', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 42, 763578, 'rare9', 'Rare', 'furni', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (false, 51, 763579, 'xmas17hats', '2018 Party Hat Boxes', 'furni', 11);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 91, 763580, 'ny17_old', 'New Year Comeback', 'furni', 12);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 42, 763581, 'xmas17gold', 'RARE Gold Accessory Pack', 'furni', 13);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 145, 763582, 'ler', 'Limited Rares', 'furni', 14);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 112, -1, 'furni_indoor_furni', 'Indoor Furni', 'furni', 15);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763584, 'indoor_top_picks', 'Top Picks', 'furni_indoor_furni', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 111, 763585, 'dynamic_chairs', 'Seating', 'furni_indoor_furni', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 112, 763586, 'dynamic_tables', 'Tables', 'furni_indoor_furni', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 114, 763587, 'dynamic_beds', 'Beds', 'furni_indoor_furni', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 113, 763588, 'dynamic_dividers', 'Dividers', 'furni_indoor_furni', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 115, 763589, 'set_dimmer', 'Lighting', 'furni_indoor_furni', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 219, 763590, 'set_posters', 'Wall decorations', 'furni_indoor_furni', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 120, 763591, 'set_teleports', 'Teleports', 'furni_indoor_furni', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 222, 763592, 'set_rollers', 'Rollers', 'furni_indoor_furni', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 224, 763593, 'badge_display', 'Badge Display', 'furni_indoor_furni', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 223, 763594, 'palooza_tents', 'Tents', 'furni_indoor_furni', 11);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 221, 763595, 'set_trophies', 'Trophies', 'furni_indoor_furni', 12);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 63, -1, 'furni_room_building', 'Room Building', 'furni', 16);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763597, 'room_building_top_picks', 'Top Picks', 'furni_room_building', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 41, 763598, 'room_building_floors', 'Floors', 'furni_room_building', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 122, 763599, 'dynamic_walls', 'Walls', 'furni_room_building', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 116, 763600, 'set_rugs', 'Carpets', 'furni_room_building', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 55, 763601, 'room_building_doors', 'Doors', 'furni_room_building', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 120, 763602, 'set_teleports2', 'Teleports', 'furni_room_building', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 218, 763603, 'set_windows', 'Windows', 'furni_room_building', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 225, 763604, 'spaces_grid', 'Spaces', 'furni_room_building', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 220, -1, 'furni_outdoor_furni', 'Outdoor Furni', 'furni', 17);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763606, 'outdoor_furni_top_picks', 'Top Picks', 'furni_outdoor_furni', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 220, 763607, 'set_plants', 'Vegetation', 'furni_outdoor_furni', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 42, 763608, 'outdoor_furni_water_features', 'Water Features', 'furni_outdoor_furni', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 21, 763609, 'outdoor_furni_ground', 'Ground', 'furni_outdoor_furni', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 206, 763610, 'outdoor_furni_backgrounds', 'Backgrounds', 'furni_outdoor_furni', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 197, -1, 'category_furni_sets', 'Furni By Line', 'furni', 18);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763612, 'furni_sub_top_picks', 'Top Picks', 'category_furni_sets', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 11, 763613, 'set_extras', 'Accessories', 'category_furni_sets', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 104, 763617, 'set_anna', 'Anna', 'category_furni_sets', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 179, 763622, 'set_pixelnew', 'Base', 'category_furni_sets', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 17, 763623, 'set_bath', 'Bathroom', 'category_furni_sets', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 21, 763631, 'set_picnic', 'Country', 'category_furni_sets', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 100, 763632, 'set_cubie', 'Cubie', 'category_furni_sets', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 204, 763633, 'set_diner_fastfood', 'Diner', 'category_furni_sets', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 157, 763636, 'bc_gardening', 'Gardening', 'category_furni_sets', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 230, 763643, 'bc_university', 'Habbo University', 'category_furni_sets', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 211, 763644, 'healthspa', 'Health Spa', 'category_furni_sets', 11);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 72, 763645, 'set_iced', 'Iced', 'category_furni_sets', 12);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 217, 763650, 'ktchn15', 'Kitchen', 'category_furni_sets', 13);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 37, 763652, 'set_lodge', 'Lodge', 'category_furni_sets', 14);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 39, 763655, 'set_mode', 'Mode', 'category_furni_sets', 15);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 41, 763658, 'set_neon', 'Neon', 'category_furni_sets', 16);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 57, 763663, 'set_pool', 'Pool', 'category_furni_sets', 17);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 48, 763665, 'set_pura', 'Pura', 'category_furni_sets', 18);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 129, 763681, 'set_usva', 'USVA', 'category_furni_sets', 19);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 172, -1, 'habbo_club', 'Habbo Club', 'furni', 19);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 214, 763687, 'club_gifts', 'Club Gifts', 'habbo_club', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 258, 763688, 'club_shop', 'HC Furni', 'habbo_club', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 125, 763689, 'set_blackhole', 'Custom Rooms', 'habbo_club', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 80, -1, 'category_wired', 'Wired', 'furni', 20);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763691, 'wired_top_picks', 'Top Picks', 'category_wired', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 84, 763692, 'wired_deals', 'Deals', 'category_wired', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 81, 763693, 'wired_triggers', 'Triggers', 'category_wired', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 82, 763694, 'wired_effects', 'Effects', 'category_wired', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 83, 763695, 'wired_conditions', 'Conditions', 'category_wired', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 85, 763696, 'wired_addons', 'Add-Ons', 'category_wired', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 4, 763697, 'sound_fx', 'Sound FX', 'category_wired', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 256, 763698, 'wired_leaderboard', 'Leaderboards', 'category_wired', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 42, 763699, 'wired_info', 'How To Use Wired', 'category_wired', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 65, 763700, 'bots', 'Bots', 'furni', 21);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 202, -1, 'category_games', 'Game Shop', 'furni', 22);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763702, 'games_top_picks', 'Top Picks', 'category_games', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 42, 763703, 'gameset_battle_banzai_info', 'How To Play Banzai', 'category_games', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 78, 763704, 'gameset_battle_banzai', 'Battle Banzai', 'category_games', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 42, 763705, 'gameset_freeze_info', 'How To Play Freeze!', 'category_games', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 87, 763706, 'gameset_freeze', 'Freeze!', 'category_games', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 56, 763707, 'gameset_football', 'Football', 'category_games', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 140, 763708, 'gameset_snowboard', 'Snowboarding', 'category_games', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 88, 763709, 'gameset_ice_hockey', 'Ice Hockey', 'category_games', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 94, 763710, 'gameset_roller_skates', 'Rollerskating', 'category_games', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 86, 763711, 'gameset_ice_tagging', 'Ice Tag', 'category_games', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 203, 763712, 'guild_frontpage', 'Habbo Groups', 'furni', 23);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 28, 763713, 'guild_custom_furni', 'Group Furni', 'guild_frontpage', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 207, 763714, 'guild_forum', 'Groups Forums', 'guild_frontpage', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 4, 763715, 'trax_jukebox', 'Music Shop', 'furni', 24);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 257, 763716, 'trax_songs', 'Habbo Hotel Hits', 'trax_jukebox', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 69, 763717, 'marketplace_info', 'Marketplace', 'furni', 25);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 70, 763718, 'martketplace_my_sales', 'My Sales', 'marketplace_info', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 71, 763719, 'marketplace_offers', 'Offers', 'marketplace_info', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, -1, 'clothing', 'Clothing', 'root', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763722, 'clothing_top_picks', 'Top Picks', 'clothing', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 215, 763723, 'clothing_new', 'New Additions', 'clothing', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763724, 'clothing_outfits', 'Outfits', 'clothing', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763725, 'clothing_hairstyles', 'Hairdos', 'clothing', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763726, 'clothing_hats', 'Hats', 'clothing', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763727, 'clothing_accessories', 'Accessories', 'clothing', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763728, 'clothing_dresses', 'Dresses', 'clothing', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763729, 'clothing_shirts', 'Shirts', 'clothing', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763730, 'clothing_jackets', 'Jackets', 'clothing', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763731, 'clothing_trousers', 'Trousers', 'clothing', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763732, 'clothing_skirts', 'Skirts', 'clothing', 11);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 74, 763733, 'clothing_shoes', 'Shoes', 'clothing', 12);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 61, 763734, 'ny2015_permeffects', 'Effects', 'clothing', 13);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 263, 763735, 'temp_effects', 'Temporary Effects', 'clothing', 14);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 24, -1, 'pets_new', 'Pets', 'root', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 8, 763737, 'pets_info', 'Pet Animals', 'pets_new', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 132, 763738, 'pet_horse', 'Pet Horse', 'pets_info', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 128, 763739, 'pet_monkey', 'Pet Monkey', 'pets_info', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 260, 763740, 'pet_raptor', 'Velociraptor Pet', 'pets_info', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 260, 763741, 'pet_pterosaur', 'Pterodactyl Pet', 'pets_info', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 68, 763742, 'pet_polar_bear', 'Bears', 'pets_info', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 109, 763743, 'pet_dragon', 'Dragons', 'pets_info', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 66, 763744, 'pet_terrier', 'Terriers', 'pets_info', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 76, 763745, 'pet_lion', 'Lion', 'pets_info', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 126, 763746, 'pet_turtle', 'Turtles', 'pets_info', 9);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 22, 763747, 'pet_crocodile', 'Crocs', 'pets_info', 10);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 77, 763748, 'pet_rhino', 'Rhino', 'pets_info', 11);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 95, 763749, 'pet_spider', 'Spider', 'pets_info', 12);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 24, 763750, 'pet_dog', 'Dogs', 'pets_info', 13);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 20, 763751, 'pet_cat', 'Cats', 'pets_info', 14);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 67, 763752, 'pet_boar', 'Pigs', 'pets_info', 15);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 97, 763753, 'pet_frog', 'Frog', 'pets_info', 16);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 228, 763755, 'babypets_info', 'Baby Pet Animals', 'pets_new', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 228, 763756, 'pet_puppy', 'Puppies', 'babypets_info', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 228, 763757, 'pet_kitten', 'Kittens', 'babypets_info', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 228, 763758, 'pet_piglet', 'Piglets', 'babypets_info', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 226, -1, 'pets_pet_equipment', 'Pet Equipment', 'pets_new', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 195, 763761, 'pet_equipment_top_picks', 'Top Picks', 'pets_pet_equipment', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 201, 763762, 'pet_accessories', 'Pet Food', 'pets_pet_equipment', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 199, 763763, 'baby_pet_accessories', 'Toys and Accessories', 'pets_pet_equipment', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 135, 763764, 'horse_saddle', 'Saddles', 'pets_pet_equipment', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 133, 763765, 'horse_dyes', 'Dyes', 'pets_pet_equipment', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 134, 763766, 'horse_styles', 'Hair Styles', 'pets_pet_equipment', 6);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 226, 763767, 'horse_jump_pilot', 'Horse Jump', 'pets_pet_equipment', 7);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 128, 763768, 'monkey_furni', 'Monkey Furni', 'pets_pet_equipment', 8);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 99, 763769, 'babypets_all', 'Breeding Boxes', 'pets_new', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 153, 763770, 'monster_plants_info', 'Monster Plants', 'pets_new', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 156, 763771, 'monster_plant_seeds', 'Monster Plant Seeds', 'monster_plants_info', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 155, 763772, 'monster_plant_potions', 'Monster Plant Potions', 'monster_plants_info', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 172, -1, 'habbo_club_desktop', 'Memberships', 'root', 5);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 172, 763774, 'hc_membership', 'Habbo Club', 'habbo_club_desktop', 1);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 193, 763775, 'builders_club_frontpage', 'Builders Club', 'habbo_club_desktop', 2);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 193, 763776, 'loyalty_bc', 'Builders Club (Diamonds)', 'habbo_club_desktop', 3);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 194, 763777, 'builders_upgrades', 'BC Upgrades', 'habbo_club_desktop', 4);
INSERT INTO catalog_node (visible, icon, id, name, caption, parent, "order") VALUES (true, 99, 763778, 'bchc_boxes', 'BC/HC Gift Boxes', 'habbo_club_desktop', 5);

